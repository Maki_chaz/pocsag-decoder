#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import argparse
import threading
from pylibs import *

def arg_parser():
    """
    :return: Return parsed args
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--ip", type=str, help="IP address to listen (GQRX default ip address: 127.0.0.1)", default="127.0.0.1")
    parser.add_argument("-p", "--port", type=int, help="Port to listen (GQRX default port: 7355)", default="7355")
    args = parser.parse_args()
    return args

if  __name__ == '__main__':
    args = arg_parser()
    bonjour()

    try:
        threading.Thread(target=netcat_udp, args=(args.ip, args.port)).start()
        threading.Thread(target=raw2decodable).start()
    except:
        print("[-] Error. Threading issues.")
